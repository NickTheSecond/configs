#!/bin/bash

git config credential.helper store
git config --global user.email "njarmusz@mymail.mines.edu"
git config --global user.name "njarmusz"
git config --global http.postBuffer 2097152000
git config --global https.postBuffer 2097152000

